import java.util.Scanner;

public class CesarEncryption {
	private Scanner sc = new Scanner(System.in);
	private static final int STANDARD_SHIFT = 2;

	public CesarEncryption() {
		super();
	}

	public CesarEncryption(Scanner sc) {
		super();
		this.sc = sc;
	}

	public Scanner getSc() {
		return sc;
	}

	public void setSc(Scanner sc) {
		this.sc = sc;
	}

	public String getCrypticText() {

		String crypticText = "";
		System.out.println("Geben Sie den zu verschlüsselnden Text ein");
		crypticText = sc.nextLine();
		return crypticText;
	}

	/*
	 * public String doEncrypt() { int shift = getShift(); return
	 * doEncrypt(decryptedText, shift);
	 */
	// }
	public String doEncrypt(String crypticText, int shift) {
		char letter;
		String decryptedText = "";
		String resultText = "";
		char[] result = new char[crypticText.length()];
		for (int i = 0; i < crypticText.length(); i++) {
			letter = crypticText.charAt(i);
			if (letter >= 'a' && letter <= 'z') {
				letter = (char) (letter + shift);
				if (letter > 'z') {
					letter = (char) (letter + 'a' - 'z' - 1);
				}
				result[i] = letter;
				resultText = new String (result);
			}
		}
		return resultText;
	}

	public int getShift() {
		System.out.println("Um wieviele Stellen wollen Sie den Text verschieben?");
		int shift = sc.nextInt();
		return shift;
	}

	
}
