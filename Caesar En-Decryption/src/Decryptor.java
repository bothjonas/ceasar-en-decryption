
import java.util.Scanner;

public interface Decryptor {
	public int getShift();
	public String getCrypticText();
}