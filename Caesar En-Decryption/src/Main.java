import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String text = "";
		String result = "";
		CesarEncryption enc = new CesarEncryption();
		
		text = enc.getCrypticText();
		int shift = enc.getShift();
		result = enc.doEncrypt(text,shift);
		System.out.println(result);
		
		
	}

}
